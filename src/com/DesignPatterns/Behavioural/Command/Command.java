package com.DesignPatterns.Behavioural.Command;

//Interface implemented by all concrete
//command classes
public interface Command {
    void execute();
}
